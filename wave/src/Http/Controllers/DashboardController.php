<?php

namespace Wave\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;

class DashboardController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $finalList = null;

        $lists = DB::table('searches')
            ->where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')->paginate(5);


        $lists->getCollection()->transform(function ($list) {

            $this->listId = $list->id;
            $totalAccounts = DB::table('results_pre')->where('job_id', $list->id)->count();
            $processedAccounts = DB::table('results')->select()->whereIn('user_id', function($query) {
                $query->select('user_id')->from('results_pre')->where('job_id', $this->listId);
            })->count();

            $complete = @round(($processedAccounts / $totalAccounts) * 100);
            $list->complete = $complete;

            return($list);

        });

        $foundEmails = DB::table('searches')->where('user_id', Auth::id())->sum('found_emails');
        $creditsRemaining = auth()->user()->role->lookups_allowed - Auth::user()->month_usage;

        return view('theme::dashboard.index')->with(['lists' => $lists, 'foundEmails' => $foundEmails, 'creditsRemaining' => $creditsRemaining]);

    }

}
