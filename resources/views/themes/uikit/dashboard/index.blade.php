@extends('theme::layouts.app')

@section('content')

	<div class="uk-container uk-text-center">

		<div class="uk-child-width-1-2@m uk-grid-match uk-margin-top" uk-grid>
			<div>
				<div class="uk-card uk-card-default">
				    <div class="uk-card-header">
				        <div class="uk-grid-small uk-flex-middle" uk-grid>
{{--				            <div class="uk-width-auto">--}}
{{--				                <div uk-icon="icon: happy; ratio: 2.8" class="welcome-icon"></div>--}}
{{--				            </div>--}}

				            <div class="uk-width-expand">
								<h3 style="display: inline" class="uk-card-title uk-margin-remove-bottom uk-blue">Emails Found</h3>
								<img class="svg-icon uk-position-right"src="http://127.0.0.1:8000/themes/uikit/svg/search.svg">
								<p class="uk-margin-remove-top uk-padding-small">The amount of emails found for you by KissLeads</p>
				            </div>
				        </div>
				    </div>
				    <div class="uk-card-body" style="padding: 25px 25px;">
				        <h1 class="dashboard-card">{{ $foundEmails }}
{{--							<img class="svg-icon" src="http://127.0.0.1:8000/themes/uikit/svg/letter.svg">--}}
						</h1>
				    </div>
				    <div class="uk-card-footer">
				        <a href="/docs" target="_blank" class="uk-button uk-button-text" id="searchButton">Make A New Search</a>
				    </div>
				</div>
			</div>

			<div>
				<div class="uk-card uk-card-default">
					<div class="uk-card-header">
						<div class="uk-grid-small uk-flex-middle" uk-grid>
							<div class="uk-width-expand">
								<h3 style="display: inline" class="uk-card-title uk-margin-remove-bottom uk-blue">Email Credits</h3>
								<img class="svg-icon uk-position-right"src="http://127.0.0.1:8000/themes/uikit/svg/diamond.svg">
								<p class="uk-margin-remove-top uk-padding-small">Your emails left for this month</p>
							</div>
						</div>
					</div>
					<div class="uk-card-body" style="padding: 25px 25px;">
						<h1 class="dashboard-card">{{ $creditsRemaining }}
{{--							<img class="svg-icon" src="http://127.0.0.1:8000/themes/uikit/svg/diamond.svg">--}}
						</h1>
					</div>
					<div class="uk-card-footer">
						<a href="/docs" target="_blank" class="uk-button uk-button-text">Get More Credits</a>
					</div>
				</div>
			</div>


		</div>

		<div class="uk-child-width-1-1@s uk-card uk-card-default">

			<table class="uk-table uk-table-middle uk-table-divider uk-margin-top center uk-table-hover" style="text-align:center;">
				<thead>
				<tr>
					<th>Added</th>
					<th>List Name</th>
					<th>Leads Found</th>
					<th>Download</th>
				</tr>
				</thead>

				<tbody>

				@foreach ($lists as $list)

					<tr>
						<td>{{ \Carbon\Carbon::parse($list->created_at)->diffForHumans() }}</td>
						<td><span uk-tooltip="title: List based on: {{ "@" . implode(", @", json_decode($list->usernames)) }} ;pos: top-right">{{ $list->list_name }}</span></td>
						@if ($list->status == 0)
							<td><progress uk-tooltip="title: {{ $list->complete }}% complete;pos: top-right" id="js-progressbar" class="uk-progress" value="{{ $list->complete }}" max="100"></progress></td>
							<td>Currently Searching</td>
						@else
							<td>Found {{ number_format($list->found_leads) }} Leads</td>
							@if ($list->found_leads > 1)
								<td><a download="{{ $list->list_name }}.csv" href="{{ asset('storage/data/' . $list->filename ) }}"><button class="uk-button uk-button-primary" type="button" style="font-size: 10px;">Download</button></a></td>
							@else
								<td>N/A</td>
							@endif
						@endif
					</tr>

				@endforeach

				</tbody>

			</table>

			{{ $lists->links() }}

		</div>

	</div>

	<div id="my-id" uk-modal>
		<div class="uk-modal-dialog">
			<button class="uk-modal-close-default" type="button" uk-close></button>
{{--			<div class="uk-modal-header">--}}
{{--				<h2 class="uk-modal-title">Search Type</h2>--}}
{{--			</div>--}}
			<div class="uk-modal-body uk-text-center">

				<div onclick="location.href='find/single';" class="uk-card uk-card-default uk-card-body uk-width-1-1@m uk-card-hover modal-border">
					<h3 class="uk-card-title">Single Search</h3>
					<img src="https://sparkpr.com/assets/illustrations/programmer.svg" class="svg-popup">
					<p>Find leads related to just <a>one</a> username</p>
				</div>

				<div onclick="location.href='find/bulk';" class="uk-card uk-card-default uk-card-body uk-width-1-1@m uk-card-hover modal-border" style="margin-top: 20px">
					<h3 class="uk-card-title">Bulk Search</h3>
					<img src="https://sparkpr.com/assets/illustrations/community.svg" class="svg-popup">
					<p>Find leads related to a <a>list</a> of usernames</p>
				</div>

			</div>
{{--			<div class="uk-modal-footer"></div>--}}
		</div>
	</div>

	<script>

		$( "#searchButton" ).click(function(e) {
			e.preventDefault();
			UIkit.modal("#my-id").show();
		});

	</script>

@endsection