@extends('theme::layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/rSlider.js') }}"></script>
<link rel="stylesheet" href="{{ URL::asset('css/rSlider.css') }}" />

@include('modals.upgrade')

@section('content')

    <div class="uk-container-small uk-align-center">
        <div class="uk-card uk-card-default">
            <div class="uk-card-header uk-text-center">
                <div class="uk-grid-small uk-flex-middle uk-grid" uk-grid="">
                    <div class="uk-width-expand">
                        <h3 class="uk-card-title uk-margin-remove-bottom uk-blue">Find New Leads</h3>
                        <p class="uk-width-2-2 uk-text-center" class="uk-text-meta uk-margin-remove-top">Our algorithms analyze the pages you submit and find <a>emails</a>, <a>phone numbers</a> and a bunch of other <a>useful details</a> automatically!</p>
                        <img class="svg-popup uk-text-center" style="width: 50px" src="http://127.0.0.1:8000/themes/uikit/svg/target.svg">
                    </div>
                </div>
            </div>

            <div class="uk-card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div class="uk-alert-warning" uk-alert>
                                <a class="uk-alert-close" uk-close></a>
                                <p>{!! $error !!}</p>
                            </div>
                        @endforeach
                    </div>
                @endif

                @if ($list = DB::table('searches')->where('user_id', Auth::id())->where('status', 0)->latest()->first())

                    <div id="searching-modal" uk-modal>
                        <div class="uk-card uk-card-default uk-card-body uk-width-1-3@m uk-card-primary uk-position-center uk-text-center">
                            <h3 class="uk-card-title">Searching...</h3>
                            <p class="popup-text">KissLeads is searching millions of accounts for emails and phone numbers relating to your list: <strong>{{ $list->list_name }}</strong></p>
                            <img src="https://loading.io/spinners/dna/index.dna-spin-spiral-preloader.svg">
                            <p class="popup-text" style="margin-bottom: 30px;">
                                We will email <b>{{ Auth::user()->email }}</b> when it is ready to download.
                            </p>
                            <button onclick="location.href='/dashboard'" class="uk-button uk-button-secondary">Awesome</button>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function() {
                            UIkit.modal('#searching-modal').show();
                        })
                    </script>

                @else (session('searchStarted'))

                    <form id="uploadForm" method="POST" action="{{ action('ListUpload@store') }}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="uk-margin">
                            <input value="{{ old('list_name') }}" name="list_name" style="resize: none; padding: 25px;" class="uk-input" type="text" placeholder="Enter a list title">
                        </div>

                        @if (Request::is('find/bulk'))

                            <textarea name="usernames" style="resize: none; padding: 25px;" class="uk-textarea" rows="5" placeholder="Enter a list of usernames, separated by a newline">{{ @implode("\r\n", old('usernames')) }}</textarea>

                        @else

                            <div class="uk-margin">
                                <input value="" name="usernames" style="resize: none; padding: 25px;" class="uk-input" type="text" placeholder="Enter a username, eg: @grantcardone">
                            </div>

                        @endif

                        <div style="margin-top: 40px; margin-bottom: 40px;">
                            <div class="uk-margin">
                                <input class="uk-checkbox" name="includeVerified" type="checkbox" style="margin-right: 10px"><label class="uk-text-left small-text">Include verified accounts</label>
                            </div>
                            <div class="uk-margin">
                                <input class="uk-checkbox" name="includeNoEmail" type="checkbox" style="margin-right: 10px"><label class="uk-text-left small-text">Include accounts without email</label>
                            </div>
                        </div>

                        <div class="uk-margin uk-width-1-1" style="margin-top: 30px !important; margin-bottom: 40px;">
                            <label class="uk-text-left small-text" style="padding: initial;">Follower Range:</label>
                            <input type="text" id="followersRange" />
                        </div>

                        <div class="uk-card-footer uk-text-center">
                            <button type="submit" class="uk-button uk-button-primary">Start Search</button>
                        </div>

                        <input id="chosenRange" name="chosenRange" type="hidden">

                    </form>

                @endif

            </div>

        </div>
    </div>

    @if (session('downloadLink'))
        <script>
            $(function() {
                setTimeout(function () {
                    document.getElementById('doDownload').click();
                }, 1500);
            });
        </script>
    @endif

    <script>
        var mySlider = new rSlider({
            target: '#followersRange',
            values: [1000, 5000, 10000, 25000, 50000, 100000, 250000, 500000, 750000, 1000000, 1500000, 2000000],
            step: 1000,
            range: true,
            tooltip: false,
            scale: true,
            labels: true,
            set: [10000, 1000000],
            onChange: function (range) {
                document.getElementById("chosenRange").value = range;
            }
        });

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        $(function() {
            renderThousands();
        });

        function renderThousands() {
            $('.rs-tooltip, ins').each(function () {
                var a = this;
                var b = $(a).text();
                $(a).text((addCommas(b)));
            });
        }

    </script>

@endsection