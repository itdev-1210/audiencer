<?php

namespace App\Traits;

use DB;

trait FormatUser {

    public function getHashtags($timeline) {

        foreach ($timeline as $item) {

            $hashtags = null;

            @preg_match_all('/(?:#)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)/i', $item->caption->text, $hashtags);
            if (empty($hashtags[0])) continue;

            $hashtags = array_unique(array_map('strtolower', $hashtags[0]));
            $foundHashtags = @array_merge((array)$foundHashtags, (array)$hashtags);

        }

        if (empty($foundHashtags)) return null;

        foreach ($foundHashtags as $hashtag)
        {
            @$rankedHashtags[$hashtag] = ($rankedHashtags[$hashtag] + 1);
        }

        arsort($rankedHashtags);

        // Now extract a maximum of 3 hashtags, which are used more than 4 times:

        foreach ($rankedHashtags as $hashtag => $value)
        {
            if ($value >= 4) $finalHashtags[] = $hashtag; // Include hashtags mentioned 4+ times
            if (@count($finalHashtags) == 3) break; // Stop at first 3
        }

        if (!empty($finalHashtags)) {
            return implode(" ", $finalHashtags);
        } else {
            return null;
        }

    }

    public function getAverageLikes($timeline) {

        foreach ($timeline as $item)
        {
            @$counter++;
            @$likeCounter = $item->like_count + $likeCounter;
        }

        if (!empty($counter)) {
            $averageLikes = round($likeCounter / $counter);
            return $averageLikes;
        } else {
            return 0;
        }

    }

    public function extractEmail($user) {

        if (preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}\b/i', $user, $email)) {
            return strtolower($email[0]);
        } else {
            return null;
        }

    }

    public function extractName($fullName) {

        if (stristr($fullName, " ")) {
            $firstName = explode(" ", $fullName);
            $firstName = $firstName[0];
        } else {
            $firstName = $fullName;
        }

        $gender = DB::table('_gendercheck')->where('name', trim($firstName))->pluck('name')->first();

        if ($gender) {
            return ucfirst($firstName);
        } else {
            return null;
        }

    }

}