<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\FormatUser;
use Log;
use DB;

class TestInstagram extends Controller
{

    use FormatUser;

    public $ig;
    public $userIds;

    function start() {

        $username = 'dogs_of_world_';

        $this->getWorker();
        $this->getSuggested($username);
        $this->getUsers();

    }

    function getUsers() {

        shuffle($this->userIds);

        foreach ($this->userIds as $userId) {

            if (DB::table('results' )->select()->where('user_id', $userId)->get()->isEmpty()) {

                $user = json_decode($this->ig->people->getInfoById($userId));
                $feed = json_decode($this->ig->timeline->getUserFeed($userId));

                $this->saveUser($user->user, $feed->items);

            }

        }

    }

    function saveUser($user, $feed) {

        if (empty($user->public_email)) $user->public_email = $this->extractEmail($user->biography);
        $average_likes = $this->getAverageLikes($feed);

        DB::table('results')->insert([
            'username' => $user->username,
            'user_id' => $user->pk,
            'full_name' => $user->full_name,
            'first_name' => $this->extractName($user->full_name),
            'bio' => $user->biography,
            'url' => $user->external_url,
            'followers' => $user->follower_count,
            'average_likes' => $average_likes,
            'engagement_rate' => round(($average_likes / $user->follower_count) * 100, 2),
            'email' => @$user->public_email,
            'phone_number' => @$user->public_phone_number,
            'category' => @$user->category,
            'top_hashtags' => $this->getHashtags($feed),
            'profile_pic_url' => $user->profile_pic_url,
            'is_verified' => $user->is_verified,
            'is_private' => $user->is_private,
            'is_business' => $user->is_business,
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        sleep(5);

    }

    function getWorker() {

        selectWorker:

        $worker = DB::table('instagram_workers')
            ->where('status', '1')
            ->where('last_request', '>', '1')
            ->orderBy('last_request', 'asc')
            ->limit(1)->get();

        if ($worker->isEmpty()) {
            Log::info("waiting for worker");
            sleep(5);
            goto selectWorker;
        } else {
            Log::info("selected worker: " . $worker['0']->id);
        }

        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $ig = new \InstagramAPI\Instagram();
        $ig->login($worker['0']->username, $worker['0']->password);

        $this->ig = $ig;

    }

    function getSuggested($username) {

        $user = $this->ig->people->getInfoByName($username);
        $user = $user->getUser();

        $suggested = $this->ig->people->getSuggestedUsers($user->getPk());
        $suggested = json_decode($suggested, true);

        foreach ($suggested['users'] as $user) {

            if ($user['is_private'] == false ) {
                $publicUsers[] = array("user_id" => $user['pk']);
            }

        }

        DB::table('results_pre')->insert($publicUsers);

        $this->userIds = array_column($publicUsers, 'user_id');

    }

}