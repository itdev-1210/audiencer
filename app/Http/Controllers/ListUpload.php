<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use View;
use DB;
use Illuminate\Http\Request;
use App\Rules\ValidateList;
use App\Searches;
use App\Jobs\SearchInstagram;

class ListUpload extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        if ($request['includeVerified'] == 'on') {
            $request['includeVerified'] = 1;
        } else {
            $request['includeVerified'] = 0;
        }

        if ($request['includeNoEmail'] == 'on') {
            $request['includeNoEmail'] = 1;
        } else {
            $request['includeNoEmail'] = 0;
        }

        $request['chosenRange'] = explode(",", $request['chosenRange']);
        $request['usernames'] = array_filter(array_unique(explode("\r\n", strtolower($request->usernames))));
        $request['usernames'] = $this->extractUsername($request['usernames']);

        $request->validate([
            'includeVerified' => ['required', 'integer'],
            'includeNoEmail' => ['required', 'integer'],
            'chosenRange' => ['required', 'array'],
            'usernames'   => ['required', 'array', new ValidateList],
            'list_name'   => ['required', 'string', 'max:100']
        ]);

        // Get remaining credits:
//        $creditsRemaining = auth()->user()->role->lookups_allowed - Auth::user()->month_usage;

//        if ($creditsRemaining <= 0) {
//            Session::flash('upgradeRequired');
//            return back()->withErrors('You do not have enough credits. Upgrade now.');
//        } else {
//            $phoneNumbers = DB::table('pages')
//                ->whereIn('id', [1, 2, 3])
//                ->limit($creditsRemaining)
//                ->pluck('body');
//        }

        $search = new Searches;
        $search->user_id = Auth::id();
        $search->list_name = $request->list_name;
        $search->usernames = $request->usernames;
        $search->include_no_email = $request->includeNoEmail;
        $search->include_verified = $request->includeVerified;
        $search->followers_min = $request->chosenRange['0'];
        $search->followers_max = $request->chosenRange['1'];
        $search->filename = md5(uniqid(rand(), true)) . ".csv";
        $search->save();

        SearchInstagram::dispatch($search->id);

//        $user = Auth::user();
//        $user->month_usage = ($user->month_usage + count($hashedPhoneNumbers));
//
//        if ($user->month_usage > auth()->user()->role->lookups_allowed) {
//
//            // Avoid 502/500 credits used scenarios when we have topped up credits.
//            $user->month_usage = auth()->user()->role->lookups_allowed;
//        }
//
//        $user->save();

        //return back()->with('success', 'Done!');

        return back();

    }

    public function extractUsername($usernames) {

        $regex = '/^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}/im';
        $users = null;

        foreach ($usernames as $username) {

            $username = str_replace("https://", "", $username);
            $username = str_replace("http://", "", $username);
            $username = str_replace("@", "", $username);

            if (stristr($username, "/")) {
                $username = explode("/", $username);
                $username = $username[1];
            }

            if (preg_match($regex, $username, $found)) {
                $users[] = strtolower($found[0]);
            }

        }

        return @array_unique($users);

    }

}