<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Authentication routes
Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// Include Wave Routes
Wave::routes();

Route::view('find/single', 'theme::upload')->middleware('auth');
Route::view('find/bulk', 'theme::upload')->middleware('auth');

Route::post('upload', [
    'uses' => 'ListUpload@store'
])->middleware('auth');

Route::get('/test', ['uses' => 'TestInstagram@start']);